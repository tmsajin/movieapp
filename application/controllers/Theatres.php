<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Theatres extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Theatres_model');
		$this->load->library('form_validation');
		if (!$this->session->userdata('login_session')) {
			redirect('login');
		}
	}

	public function index()
	{
		$q = urldecode($this->input->get('q', TRUE));
		$start = intval($this->input->get('start'));

		if ($q <> '') {
			$config['base_url'] = base_url() . 'theatres/index.html?q=' . urlencode($q);
			$config['first_url'] = base_url() . 'theatres/index.html?q=' . urlencode($q);
		} else {
			$config['base_url'] = base_url() . 'theatres/index.html';
			$config['first_url'] = base_url() . 'theatres/index.html';
		}

		$config['per_page'] = PER_PAGE_ITEMS;
		$config['page_query_string'] = TRUE;
		$config['total_rows'] = $this->Theatres_model->total_rows($q);
		$theatres = $this->Theatres_model->get_limit_data($config['per_page'], $start, $q);

		$this->load->library('pagination');
		$this->pagination->initialize($config);

		$data = array(
			'theatres_data' => $theatres,
			'q' => $q,
			'pagination' => $this->pagination->create_links(),
			'total_rows' => $config['total_rows'],
			'start' => $start,
		);
		$this->load->view('header', $data);
		$this->load->view('theatres/theatres_list', $data);
		$this->load->view('footer', $data);
	}

	public function read($id)
	{
		$row = $this->Theatres_model->get_by_id($id);
		if ($row) {
			$data = array(
				'theatre_id' => $row->theatre_id,
				'theatre_name' => $row->theatre_name,
				'location' => $row->location,
				'address' => $row->address,
				'district' => $row->district,
				'contact_number1' => $row->contact_number1,
				'contact_number2' => $row->contact_number2,
				'created_date' => $row->created_date,
			);
			$this->load->view('header', $data);
			$this->load->view('theatres/theatres_read', $data);
			$this->load->view('footer', $data);
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('theatres'));
		}
	}

	public function create()
	{
		$t_name_data = $this->Common_model->get_autofill("theatres", "theatre_name");
		$l_data = $this->Common_model->get_autofill('theatres', 'location');
		$data = array(
			'button' => 'Create',
			'action' => site_url('theatres/create_action'),
			'theatre_id' => set_value('theatre_id'),
			'theatre_name' => set_value('theatre_name'),
			'location' => set_value('location'),
			'address' => set_value('address'),
			'district' => set_value('district'),
			'contact_number1' => set_value('contact_number1'),
			'contact_number2' => set_value('contact_number2'),
			'created_date' => set_value('created_date'),
			'theatre_type_autofill' => $t_name_data,
			'location_autofill' => $l_data,
		);
		$this->load->view('header', $data);
		$this->load->view('theatres/theatres_form', $data);
		$this->load->view('footer', $data);
	}

	public function create_action()
	{
		$this->_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->create();
		} else {
			$data = array(
				'theatre_name' => $this->input->post('theatre_name', TRUE),
				'location' => $this->input->post('location', TRUE),
				'address' => $this->input->post('address', TRUE),
				'district' => $this->input->post('district', TRUE),
				'contact_number1' => $this->input->post('contact_number1', TRUE),
				'contact_number2' => $this->input->post('contact_number2', TRUE),
				'created_date' => $this->input->post('created_date', TRUE),
			);

			$this->Theatres_model->insert($data);
			$this->session->set_flashdata('message', 'Create Record Success');
			redirect(site_url('theatres'));
		}
	}

	public function update($id)
	{
		$row = $this->Theatres_model->get_by_id($id);
		$t_name_data = $this->Common_model->get_autofill("theatres", "theatre_name");
		$l_data = $this->Common_model->get_autofill('theatres', 'location');
		if ($row) {
			$data = array(
				'button' => 'Update',
				'action' => site_url('theatres/update_action'),
				'theatre_id' => set_value('theatre_id', $row->theatre_id),
				'theatre_name' => set_value('theatre_name', $row->theatre_name),
				'location' => set_value('location', $row->location),
				'address' => set_value('address', $row->address),
				'district' => set_value('district', $row->district),
				'contact_number1' => set_value('contact_number1', $row->contact_number1),
				'contact_number2' => set_value('contact_number2', $row->contact_number2),
				'created_date' => set_value('created_date', $row->created_date),
				'theatre_type_autofill' => $t_name_data,
				'location_autofill' => $l_data,
			);
			$this->load->view('header', $data);
			$this->load->view('theatres/theatres_form', $data);
			$this->load->view('footer', $data);
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('theatres'));
		}
	}

	public function update_action()
	{
		$this->_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->update($this->input->post('theatre_id', TRUE));
		} else {
			$data = array(
				'theatre_name' => $this->input->post('theatre_name', TRUE),
				'location' => $this->input->post('location', TRUE),
				'address' => $this->input->post('address', TRUE),
				'district' => $this->input->post('district', TRUE),
				'contact_number1' => $this->input->post('contact_number1', TRUE),
				'contact_number2' => $this->input->post('contact_number2', TRUE),
				'created_date' => $this->input->post('created_date', TRUE),
			);

			$this->Theatres_model->update($this->input->post('theatre_id', TRUE), $data);
			$this->session->set_flashdata('message', 'Update Record Success');
			redirect(site_url('theatres'));
		}
	}

	public function delete($id)
	{
		$row = $this->Theatres_model->get_by_id($id);
		
		if ($row) {
			
			$status = $this->Theatres_model->delete($id);
			if(FALSE == $status){
				$message = "Error: Could not delete Theatre. Already associated to Advertisement.";
			} else {
				$message = 'Delete Record Success';
			}
			$this->session->set_flashdata('message', $message);
			redirect(site_url('theatres'));
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('theatres'));
		}
	}

	public function _rules()
	{
		$this->form_validation->set_rules('theatre_name', 'theatre name', 'trim|required');
		$this->form_validation->set_rules('location', 'location', 'trim|required');
		$this->form_validation->set_rules('district', 'district', 'trim|required');
		
		$this->form_validation->set_rules('theatre_id', 'theatre_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
	}
}

/* End of file Theatres.php */
/* Location: ./application/controllers/Theatres.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-02-09 14:56:21 */
/* http://harviacode.com */
