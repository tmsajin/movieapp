<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Advertisement_theatre extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Advertisement_theatre_model');
		$this->load->library('form_validation');
		if (!$this->session->userdata('login_session')) {
			redirect('login');
		}
	}

	public function index()
	{
		$q = urldecode($this->input->get('q', TRUE));
		$start = intval($this->input->get('start'));

		if ($q <> '') {
			$config['base_url'] = base_url() . 'advertisement_theatre/index.html?q=' . urlencode($q);
			$config['first_url'] = base_url() . 'advertisement_theatre/index.html?q=' . urlencode($q);
		} else {
			$config['base_url'] = base_url() . 'advertisement_theatre/index.html';
			$config['first_url'] = base_url() . 'advertisement_theatre/index.html';
		}

		$config['per_page'] = PER_PAGE_ITEMS;
		$config['page_query_string'] = TRUE;
		$config['total_rows'] = $this->Advertisement_theatre_model->total_rows($q);
		$advertisement_theatre = $this->Advertisement_theatre_model->get_limit_data($config['per_page'], $start, $q);
		$this->load->library('pagination');
		$this->pagination->initialize($config);

		$data = array(
			'advertisement_theatre_data' => $advertisement_theatre,
			'q' => $q,
			'pagination' => $this->pagination->create_links(),
			'total_rows' => $config['total_rows'],
			'start' => $start,
		);
		$this->load->view('header');
		$this->load->view('advertisement_theatre/advertisement_theatre_list', $data);
		$this->load->view('footer');
	}

	public function read($id)
	{
		$row = $this->Advertisement_theatre_model->get_by_id($id);
		if ($row) {
			$data = array(
				'adtheatr_id' => $row->adtheatr_id,
				'ad_id' => $row->ad_id,
				'theatre_id' => $row->theatre_id,
				'time_of_show_start' => $row->time_of_show_start,
				'time_of_show_end' => $row->time_of_show_end,
				'created' => $row->created,
			);
			$this->load->view('header');
			$this->load->view('advertisement_theatre/advertisement_theatre_read', $data);
			$this->load->view('footer');
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('advertisement_theatre'));
		}
	}

	public function create()
	{
		$t_name_data = $this->Advertisement_theatre_model->get_menu_data_theatres_with_district("theatres", "theatre_name");
		$a_data = $this->Advertisement_theatre_model->get_menu_data_advertisement('advertisements', 'ad_name');
		$data = array(
			'button' => 'Create',
			'action' => site_url('advertisement_theatre/create_action'),
			'adtheatr_id' => set_value('adtheatr_id'),
			'ad_id' => set_value('ad_id'),
			'theatre_id' => set_value('theatre_id'),
			'time_of_show_start' => set_value('time_of_show_start'),
			'time_of_show_end' => set_value('time_of_show_end'),
			'created' => set_value('created'),
			'theatre_type_autofill' => $t_name_data,
			'advertisement_autofill' => $a_data,
		);
		$this->load->view('header');
		$this->load->view('advertisement_theatre/advertisement_theatre_form', $data);
		$this->load->view('footer');
	}

	/* -------------------------------------------------------------------------- */
	/*               Assign theaters to district - bulk assign page               */
	/* -------------------------------------------------------------------------- */

	public function assign_district()
	{
		$this->assign_district_action();
		
		$t_name_data = $this->Advertisement_theatre_model->get_menu_data_theatres("theatres", "theatre_name");
		$a_data = $this->Advertisement_theatre_model->get_menu_data_advertisement('advertisements', 'ad_name') + array('' => '-Select one');
		$ad_id = $this->input->post('ad_id');
		
		
		/*
			Generate the multi dimentional array for theatres based on district
		*/
		$data = array();
		$selected_theatres = $this->Advertisement_theatre_model->get_selected_district_based_theatres($ad_id);
		
		$data['district_based_theatres_array'] = $this->Advertisement_theatre_model->get_district_based_theatres_array();
		if ($ad_id) {
			$selected_theatres_form_data = $this->input->post('district');
			if(!empty($selected_theatres_form_data) && !empty($_POST)){
				$selected_theatres = $selected_theatres_form_data;
			}
		};
		
		$data = $data + array(
			'button' => 'Create',
			'action' => site_url('advertisement_theatre/assign_district'),
			'adtheatr_id' => set_value('adtheatr_id'),
			'ad_id' => $ad_id,
			'theatre_id' => set_value('theatre_id'),
			'time_of_show_start' => set_value('time_of_show_start'),
			'time_of_show_end' => set_value('time_of_show_end'),
			'created' => set_value('created'),
			'theatre_type_autofill' => $t_name_data,
			'advertisement_autofill' => $a_data,
		);
		$data['district_based_theatres_selected'] = $selected_theatres;
		
		$this->load->view('header');
		$this->load->view('advertisement_theatre/advertisement_theatre_district_form', $data);
		$this->load->view('footer');
	}
	public function assign_district_action()
	{
		if(empty($_POST)){
			return false;
		};
		
		$this->form_validation->set_rules('ad_id', 'ad id', 'trim|required');
		$this->form_validation->set_rules('time_of_show_start', 'Time of show start', 'trim|required');
		$this->form_validation->set_rules('time_of_show_end', 'Time of show end', 'trim|required');
		$this->form_validation->set_rules('district[]', 'Districts', 'trim|required');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
		if (!$this->input->post('form_hidden_menu_change_action')=='menu_change' && $this->form_validation->run() != FALSE) {
			$data = array(
				'ad_id' => $this->input->post('ad_id', TRUE),
				'district' => $this->input->post('district', TRUE),
				'time_of_show_start' => $this->input->post('time_of_show_start', TRUE),
				'time_of_show_end' => $this->input->post('time_of_show_end', TRUE)
			);
			$this->Advertisement_theatre_model->assign_theatres($data);
			$this->session->set_flashdata('message', 'Assign to district Success');
			redirect(site_url('advertisement_theatre'));
		}
	}

	public function create_action()
	{
		$this->_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->create();
		} else {
			$data = array(
				'ad_id' => $this->input->post('ad_id', TRUE),
				'theatre_id' => $this->input->post('theatre_id', TRUE),
				'time_of_show_start' => $this->input->post('time_of_show_start', TRUE),
				'time_of_show_end' => $this->input->post('time_of_show_end', TRUE),
				'created' => $this->input->post('created', TRUE),
			);

			$this->Advertisement_theatre_model->insert($data);
			$this->session->set_flashdata('message', 'Create Record Success');
			redirect(site_url('advertisement_theatre'));
		}
	}

	public function update($id)
	{
		$row = $this->Advertisement_theatre_model->get_by_id($id);
		$t_name_data = $this->Advertisement_theatre_model->get_menu_data_theatres("theatres", "theatre_name");
		$a_data = $this->Advertisement_theatre_model->get_menu_data_advertisement('advertisements', 'ad_name');

		if ($row) {
			$data = array(
				'button' => 'Update',
				'action' => site_url('advertisement_theatre/update_action'),
				'adtheatr_id' => set_value('adtheatr_id', $row->adtheatr_id),
				'ad_id' => set_value('ad_id', $row->ad_id),
				'theatre_id' => set_value('theatre_id', $row->theatre_id),
				'time_of_show_start' => set_value('time_of_show_start', $row->time_of_show_start),
				'time_of_show_end' => set_value('time_of_show_end', $row->time_of_show_end),
				'created' => set_value('created', $row->created),
				'theatre_type_autofill' => $t_name_data,
				'advertisement_autofill' => $a_data,
			);
			$this->load->view('header');
			$this->load->view('advertisement_theatre/advertisement_theatre_form', $data);
			$this->load->view('footer');
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('advertisement_theatre'));
		}
	}

	public function update_action()
	{
		$this->_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->update($this->input->post('adtheatr_id', TRUE));
		} else {
			$data = array(
				'ad_id' => $this->input->post('ad_id', TRUE),
				'theatre_id' => $this->input->post('theatre_id', TRUE),
				'time_of_show_start' => $this->input->post('time_of_show_start', TRUE),
				'time_of_show_end' => $this->input->post('time_of_show_end', TRUE),
				'created' => $this->input->post('created', TRUE),
			);

			$this->Advertisement_theatre_model->update($this->input->post('adtheatr_id', TRUE), $data);
			$this->session->set_flashdata('message', 'Update Record Success');
			redirect(site_url('advertisement_theatre'));
		}
	}

	public function delete($id)
	{
		$row = $this->Advertisement_theatre_model->get_by_id($id);

		if ($row) {
			$this->Advertisement_theatre_model->delete($id);
			$this->session->set_flashdata('message', 'Delete Record Success');
			redirect(site_url('advertisement_theatre'));
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('advertisement_theatre'));
		}
	}

	public function _rules()
	{
		$this->form_validation->set_rules('ad_id', 'ad id', 'trim|required');
		$this->form_validation->set_rules('theatre_id', 'theatre id', 'trim|required');
		$this->form_validation->set_rules('time_of_show_start', 'date of show start', 'trim|required');
		$this->form_validation->set_rules('time_of_show_end', 'date of show end', 'trim|required');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
	}
}
