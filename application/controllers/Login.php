<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('Users_model');
		$this->load->library('form_validation');
	}
	function index()
	{
		$data['message'] = '';
		$username = ($this->input->post('username', TRUE));
		$password = ($this->input->post('password', TRUE));
		if ($username && $password) {
			$row = $this->Users_model->check_login($username, $password);
			if (is_object($row)) {
				if ($row->username == $username && $row->password == $password) {
					$array = array(
						'login_session' => $row
					);
					
					$this->session->set_userdata($array);
					redirect(base_url() . 'welcome');
					die();
				}
			}
			$data['message'] = "Invalid Login.";
		}
		$this->load->view('header_login');
		$this->load->view('login_form', $data);
		$this->load->view('footer_login');
	}
	function logout()
	{
		$this->session->set_userdata('login_session');
		redirect(base_url());
	}
}
        
    /* End of file  Login.php */
