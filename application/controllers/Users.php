<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Users extends CI_Controller
{
	public $session_data;
	function __construct()
	{
		parent::__construct();
		$this->load->model('Users_model');
		$this->load->library('form_validation');
		
		if (!$this->session->userdata('login_session') || $this->session->userdata('login_session')->user_type<>'Admin') {
			redirect('login');
		}
		$this->session_data = $this->session->userdata('login_session');
	}

	public function index()
	{
		$q = urldecode($this->input->get('q', TRUE));
		$start = intval($this->input->get('start'));

		if ($q <> '') {
			$config['base_url'] = base_url() . 'users/index.html?q=' . urlencode($q);
			$config['first_url'] = base_url() . 'users/index.html?q=' . urlencode($q);
		} else {
			$config['base_url'] = base_url() . 'users/index.html';
			$config['first_url'] = base_url() . 'users/index.html';
		}

		$config['per_page'] = PER_PAGE_ITEMS;
		$config['page_query_string'] = TRUE;
		$config['total_rows'] = $this->Users_model->total_rows($q);
		$users = $this->Users_model->get_limit_data($config['per_page'], $start, $q);

		$this->load->library('pagination');
		$this->pagination->initialize($config);

		$data = array(
			'users_data' => $users,
			'q' => $q,
			'pagination' => $this->pagination->create_links(),
			'total_rows' => $config['total_rows'],
			'start' => $start,
			'session_data'=>$this->session_data
		);
		$this->load->view('header');

		$this->load->view('users/users_list', $data);
		$this->load->view('footer');
	}

	public function read($id)
	{
		$row = $this->Users_model->get_by_id($id);
		if ($row) {
			$data = array(
				'user_id' => $row->user_id,
				'username' => $row->username,
				'password' => $row->password,
				'status' => $row->status,
				'user_type' => $row->user_type,
			);
			$this->load->view('header');

			$this->load->view('users/users_read', $data);
			$this->load->view('footer');
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('users'));
		}
	}

	public function create()
	{
		$data = array(
			'button' => 'Create',
			'action' => site_url('users/create_action'),
			'user_id' => set_value('user_id'),
			'username' => set_value('username'),
			'password' => set_value('password'),
			'status' => set_value('status'),
			'user_type' => set_value('user_type'),
		);
		$this->load->view('header');

		$this->load->view('users/users_form', $data);

		$this->load->view('footer');
	}

	public function create_action()
	{
		$this->_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->create();
		} else {
			$data = array(
				'username' => $this->input->post('username', TRUE),
				'password' => $this->input->post('password', TRUE),
				'status' => $this->input->post('status', TRUE),
				'user_type' => $this->input->post('user_type', TRUE),
			);

			$this->Users_model->insert($data);
			$this->session->set_flashdata('message', 'Create Record Success');
			redirect(site_url('users'));
		}
	}

	public function update($id)
	{
		$row = $this->Users_model->get_by_id($id);

		if ($row) {
			$data = array(
				'button' => 'Update',
				'action' => site_url('users/update_action'),
				'user_id' => set_value('user_id', $row->user_id),
				'username' => set_value('username', $row->username),
				'password' => set_value('password', $row->password),
				'status' => set_value('status', $row->status),
				'user_type' => set_value('user_type', $row->user_type),
			);
			$this->load->view('header');

			$this->load->view('users/users_form', $data);

			$this->load->view('footer');
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('users'));
		}
	}

	public function update_action()
	{
		$this->_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->update($this->input->post('user_id', TRUE));
		} else {
			$data = array(
				'username' => $this->input->post('username', TRUE),
				'password' => $this->input->post('password', TRUE),
				'status' => $this->input->post('status', TRUE),
				'user_type' => $this->input->post('user_type', TRUE),
			);

			$this->Users_model->update($this->input->post('user_id', TRUE), $data);
			$this->session->set_flashdata('message', 'Update Record Success');
			redirect(site_url('users'));
		}
	}

	public function delete($id)
	{
		$row = $this->Users_model->get_by_id($id);

		if ($row) {
			$this->Users_model->delete($id);
			$this->session->set_flashdata('message', 'Delete Record Success');
			redirect(site_url('users'));
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('users'));
		}
	}

	public function _rules()
	{
		$this->form_validation->set_rules('username', 'username', 'trim|required');
		$this->form_validation->set_rules('password', 'password', 'trim|required');
		$this->form_validation->set_rules('status', 'status', 'trim|required');
		$this->form_validation->set_rules('user_type', 'user type', 'trim|required');

		$this->form_validation->set_rules('user_id', 'user_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
	}
}
