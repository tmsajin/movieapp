<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Client extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Client_model');
		$this->load->library('form_validation');
		if (!$this->session->userdata('login_session')) {
			redirect('login');
		}
	}

	public function index()
	{
		$q = urldecode($this->input->get('q', TRUE));
		$start = intval($this->input->get('start'));

		if ($q <> '') {
			$config['base_url'] = base_url() . 'client/index.html?q=' . urlencode($q);
			$config['first_url'] = base_url() . 'client/index.html?q=' . urlencode($q);
		} else {
			$config['base_url'] = base_url() . 'client/index.html';
			$config['first_url'] = base_url() . 'client/index.html';
		}

		$config['per_page'] = PER_PAGE_ITEMS;
		$config['page_query_string'] = TRUE;
		$config['total_rows'] = $this->Client_model->total_rows($q);
		$client = $this->Client_model->get_limit_data($config['per_page'], $start, $q);

		$this->load->library('pagination');
		$this->pagination->initialize($config);

		$data = array(
			'client_data' => $client,
			'q' => $q,
			'pagination' => $this->pagination->create_links(),
			'total_rows' => $config['total_rows'],
			'start' => $start,
		);
		$this->load->view('header');
		$this->load->view('client/client_list', $data);
		$this->load->view('footer');
	}

	public function read($id)
	{
		$row = $this->Client_model->get_by_id($id);
		if ($row) {
			$data = array(
				'client_id' => $row->client_id,
				'client_name' => $row->client_name,
				'client_type' => $row->client_type,
				'client_of_agency' => $row->client_of_agency,
				'location' => $row->location,
				'address' => $row->address,
				'district' => $row->district,
				'contact_number' => $row->contact_number,
				'email_id' => $row->email_id
			);
			$this->load->view('header');
			$this->load->view('client/client_read', $data);
			$this->load->view('footer');
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('client'));
		}
	}

	public function create()
	{
		$l_data = $this->Common_model->get_autofill('client', 'location');
		$data = array(
			'button' => 'Create',
			'action' => site_url('client/create_action'),
			'client_id' => set_value('client_id'),
			'client_name' => set_value('client_name'),
			'client_type' => set_value('client_type'),
			'client_of_agency' => set_value('client_of_agency'),
			'location' => set_value('location'),
			'address' => set_value('address'),
			'district' => set_value('district'),
			'contact_number' => set_value('contact_number'),
			'email_id' => set_value('email_id'),
			'location_autofill' => $l_data,
		);
		$this->load->view('header');
		$this->load->view('client/client_form', $data);
		$this->load->view('footer');
	}

	public function create_action()
	{
		$this->_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->create();
		} else {
			$data = array(
				'client_name' => $this->input->post('client_name', TRUE),
				'client_type' => $this->input->post('client_type', TRUE),
				'client_of_agency' => $this->input->post('client_of_agency', TRUE),
				'location' => $this->input->post('location', TRUE),
				'address' => $this->input->post('address', TRUE),
				'district' => $this->input->post('district', TRUE),
				'contact_number' => $this->input->post('contact_number', TRUE),
				'email_id' => $this->input->post('email_id', TRUE),
			);
			$this->Client_model->insert($data);
			$this->session->set_flashdata('message', 'Create Record Success');
			redirect(site_url('client'));
		}
	}

	public function update($id)
	{
		$row = $this->Client_model->get_by_id($id);
		$l_data = $this->Common_model->get_autofill('client', 'location');
		if ($row) {
			$data = array(
				'button' => 'Update',
				'action' => site_url('client/update_action'),
				'client_id' => set_value('client_id', $row->client_id),
				'client_name' => set_value('client_name', $row->client_name),
				'client_type' => set_value('client_type', $row->client_type),
				'client_of_agency' => set_value('client_of_agency', $row->client_of_agency),
				'location' => set_value('location', $row->location),
				'address' => set_value('address', $row->address),
				'district' => set_value('district', $row->district),
				'contact_number' => set_value('contact_number', $row->contact_number),
				'email_id' => set_value('email_id', $row->email_id),
				'location_autofill' => $l_data,
			);
			$this->load->view('header');
			$this->load->view('client/client_form', $data);
			$this->load->view('footer');
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('client'));
		}
	}

	public function update_action()
	{
		$l_data = $this->Common_model->get_autofill('client', 'location');
		$this->_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->update($this->input->post('client_id', TRUE));
		} else {
			$data = array(
				'client_name' => $this->input->post('client_name', TRUE),
				'client_type' => $this->input->post('client_type', TRUE),
				'client_of_agency' => $this->input->post('client_of_agency', TRUE),
				'location' => $this->input->post('location', TRUE),
				'address' => $this->input->post('address', TRUE),
				'district' => $this->input->post('district', TRUE),
				'contact_number' => $this->input->post('contact_number', TRUE),
				'email_id' => $this->input->post('email_id', TRUE)
			);
			if ($this->input->post('client_of_agency', TRUE) == 'Agency') {
				$data['client_of_agency'] = $this->input->post('client_of_agency', TRUE);
			}
			$this->Client_model->update($this->input->post('client_id', TRUE), $data);
			$this->session->set_flashdata('message', 'Update Record Success');
			redirect(site_url('client'));
		}
	}

	public function delete($id)
	{
		$row = $this->Client_model->get_by_id($id);

		if ($row) {
			$status = $this->Client_model->delete($id);
			if(FALSE == $status){
				$message = "Error: Could not delete Client. Already associated to Advertisement. Delete the Advertisement first.";
			} else {
				$message = 'Delete Record Success';
			}
			$this->session->set_flashdata('message', $message);
			redirect(site_url('client'));
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('client'));
		}
	}

	public function _rules()
	{
		$this->form_validation->set_rules('client_name', 'client name', 'trim|required');
		
		$this->form_validation->set_rules('client_id', 'client_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
	}
}
