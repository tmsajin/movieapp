<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Reports_general extends CI_Controller
{
	public $search_result;
	function __construct()
	{
		parent::__construct();
		$this->load->model('Reports_model');
		$this->load->library('form_validation');
		if (!$this->session->userdata('login_session')) {
			redirect('login');
		}
	}
	public function download()
	{
		$this->load->helper('download');
		$list = $this->search_results;

		$fp = fopen('php://output', 'w');
		$output_string = array("No", "Client Name", "Ad Name", "Theatre Name", "District", "Show start date", "Show end date");
		fputcsv($fp, $output_string);
		$i = 1;
		foreach ($list as $fields) {
			$output_string = array(
				$i++, $fields->client_name, $fields->ad_name,
				$fields->theatre_name, $fields->district,
				$fields->time_of_show_start, $fields->time_of_show_end
			);
			fputcsv($fp, $output_string);
		}
		//die('asd');
		$data = file_get_contents('php://output');
		$name = 'data.csv';

		// Build the headers to push out the file properly.
		header('Pragma: public');     // required
		header('Expires: 0');         // no cache
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Cache-Control: private', false);
		header('Content-Disposition: attachment; filename="' . basename($name) . '"');  // Add the file name
		header('Content-Transfer-Encoding: binary');
		header('Connection: close');
		exit();

		force_download($name, $data);
		fclose($fp);
	}
	public function search_action()
	{
		$search_array = $_GET;

		if (isset($search_array['submit'])) {
			switch ($search_array['submit']) {
				case "Download": {
						$this->download();
						die();
						break;
					}
				case "Reset": {
						redirect(site_url('reports_general'));
						die();
						break;
					}
			}
		}
	}
	public function index()
	{
		$this->load->model('Advertisement_theatre_model');
		$this->load->model('Client_advertisement_model');
		/**get menu data for client , advertisement, theatres */
		$t_name_data = $this->Advertisement_theatre_model->get_menu_data_theatres_with_district("theatres", "theatre_name") + array('' => "-Select Theatre-");

		$a_data = $this->Advertisement_theatre_model->get_menu_data_advertisement('advertisements', 'ad_name') + array('' => "-Select Ad-");
		$client_data = $this->Client_advertisement_model->get_menu_data_client('client', 'client_name') + array('' => "-Select Client-");

		$q_client = urldecode($this->input->get('client_search', TRUE));
		$q_theatre = urldecode($this->input->get('theatre_search', TRUE));
		$q_advertisement = urldecode($this->input->get('advertisement_search', TRUE));
		$search_array = array(
			'client' => $q_client, "advertisement" => $q_advertisement, "theatre" => $q_theatre,
			'show_critical'=> $this->input->get('show_critical', TRUE), 'show_warning' => $this->input->get('show_warning', TRUE)
		);
		$start = intval($this->input->get('start'));

		if ($q_client <> '' || $q_theatre <> '' || $q_advertisement <> '') {
			$config['base_url'] = base_url() . "reports_general/?q_client=$q_client&q_theatre=$q_theatre&$q_advertisement=$q_advertisement";
			$config['first_url'] = base_url() . "reports_general/?q_client=$q_client&q_theatre=$q_theatre&$q_advertisement=$q_advertisement";
		} else {
			$config['base_url'] = base_url() . 'reports_general/';
			$config['first_url'] = base_url() . 'reports_general/';
		}

		$config['per_page'] = PER_PAGE_ITEMS;
		$config['page_query_string'] = TRUE;
		$config['total_rows'] = $this->Reports_model->total_rows($search_array);
		$this->search_results = $this->Reports_model->get_limit_data($config['per_page'], $start, $search_array);

		//echo($this->db->last_query());

		$this->search_action();

		$this->load->library('pagination');
		$this->pagination->initialize($config);

		$data = array(
			'client_advertisement_data' => $this->search_results,
			'q' => $search_array,
			'pagination' => $this->pagination->create_links(),
			'total_rows' => $config['total_rows'],
			'start' => $start,
			't_data' => $t_name_data,
			'a_data' => $a_data,
			'client_data' => $client_data
		);
		$this->load->view('header');
		$this->load->view('reports/reports_general', $data);
		$this->load->view('footer');
	}
}
