<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');


class Advertisements extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('Advertisements_model');
		$this->load->model('Client_Advertisement_model');
		$this->load->library('form_validation');
		if (!$this->session->userdata('login_session')) {
			redirect('login');
		}
	}

	public function index()
	{
		$q = urldecode($this->input->get('q', TRUE));
		$start = intval($this->input->get('start'));

		if ($q <> '') {
			$config['base_url'] = base_url() . 'advertisements/index.html?q=' . urlencode($q);
			$config['first_url'] = base_url() . 'advertisements/index.html?q=' . urlencode($q);
		} else {
			$config['base_url'] = base_url() . 'advertisements/index.html';
			$config['first_url'] = base_url() . 'advertisements/index.html';
		}

		$config['per_page'] = PER_PAGE_ITEMS;
		$config['page_query_string'] = TRUE;
		$config['total_rows'] = $this->Client_Advertisement_model->total_rows($q);
		$advertisements = $this->Client_Advertisement_model->get_limit_data($config['per_page'], $start, $q);
		
		$this->load->library('pagination');
		$this->pagination->initialize($config);

		$data = array(
			'advertisements_data' => $advertisements,
			'q' => $q,
			'pagination' => $this->pagination->create_links(),
			'total_rows' => $config['total_rows'],
			'start' => $start,
		);

		$this->load->view('header');
		$this->load->view('advertisements/advertisements_list', $data);
		$this->load->view('footer');
	}

	public function read($id)
	{
		$row = $this->Client_Advertisement_model->get_client_of_ad($id);
		//print_r($row);die();
		if ($row) {
			$data = array(
				'ad_id' => $row->ad_id,
				'ad_name' => $row->ad_name,
				'ad_type' => $row->ad_type,
				'client_ad_row' => $row,
				'created_date' => $row->created_date,
				'status' => $row->status,
			);
			$this->load->view('header');
			$this->load->view('advertisements/advertisements_read', $data);
			$this->load->view('footer');
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('advertisements'));
		}
	}

	public function create()
	{
		$get_menu_data_client = $this->Client_Advertisement_model->get_menu_data_client();

		$data = array(
			'button' => 'Create',
			'action' => site_url('advertisements/create_action'),
			'ad_id' => set_value('ad_id'),
			'ad_name' => set_value('ad_name'),
			'ad_type' => set_value('ad_type'),
			'client_id' => set_value('client_id'),
			'status' => set_value('status'),
			'autofil_data' => $this->Advertisements_model->get_ad_type_autofill(),
			'clients' => array('' => '-Select Client-') + $get_menu_data_client
		);
		$this->load->view('header');
		$this->load->view('advertisements/advertisements_form', $data);
		$this->load->view('footer');
	}

	public function create_action()
	{
		$this->_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->create();
		} else {
			$data = array(
				'ad_name' => $this->input->post('ad_name', TRUE),
				'ad_type' => $this->input->post('ad_type', TRUE),
				'status' => $this->input->post('status', TRUE),
			);
			$this->Advertisements_model->insert($data);
			$ad_id_new = $this->db->insert_id();
			/* -------------------------------------------------------------------------- */
			/*              Insert to client advertisement mapping table too              */
			/* -------------------------------------------------------------------------- */
			$data = array(
				'client_id' => $this->input->post('client_id', TRUE),
				'adversment_id' => $ad_id_new
			);

			$this->Client_Advertisement_model->delete_by_client_ad($this->input->post('client_id', TRUE), $ad_id_new);

			$this->Client_Advertisement_model->insert($data);

			$this->session->set_flashdata('message', 'Create Record Success');
			redirect(site_url('advertisements'));
		}
	}

	public function update($id)
	{
		$row = $this->Advertisements_model->get_by_id($id);
		$get_menu_data_client = $this->Client_Advertisement_model->get_menu_data_client();

		/* -------------------------------------------------------------------------- */
		/*                       find client id related to this ad                       */
		/* -------------------------------------------------------------------------- */
		$client_of_ad_row =  $this->Client_Advertisement_model->get_client_of_ad($id);

		if ($row) {
			$data = array(
				'button' => 'Update',
				'action' => site_url('advertisements/update_action'),
				'ad_id' => set_value('ad_id', $row->ad_id),
				'ad_name' => set_value('ad_name', $row->ad_name),
				'ad_type' => set_value('ad_type', $row->ad_type),
				'created_date' => set_value('created_date', $row->created_date),
				'client_id' => set_value('client_id', $client_of_ad_row->client_id),
				'status' => set_value('status', $row->status),
				'autofil_data' => $this->Advertisements_model->get_ad_type_autofill(),
				'clients' => $get_menu_data_client + array('' => '-Select Client-')
			);
			$this->load->view('header');
			$this->load->view('advertisements/advertisements_form', $data);
			$this->load->view('footer');
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('advertisements'));
		}
	}

	public function update_action()
	{
		$this->_rules();
		if ($this->form_validation->run() == FALSE) {
			$this->update($this->input->post('ad_id', TRUE));
		} else {
			$data = array(
				'ad_name' => $this->input->post('ad_name', TRUE),
				'ad_type' => $this->input->post('ad_type', TRUE),
				'created_date' => $this->input->post('created_date', TRUE),
				'status' => $this->input->post('status', TRUE),
			);
			$ad_id = $this->input->post('ad_id', TRUE);
			$this->Advertisements_model->update($ad_id, $data);
			$new_client_id = $this->input->post('client_id', TRUE);
			/* -------------------------------------------------------------------------- */
			/*              Update to client advertisement mapping table too              */
			/* -------------------------------------------------------------------------- */
			$data = array(
				'client_id' => $new_client_id,
				'adversment_id' => $ad_id
			);
			/*
				Find previous accociation with client
			*/

			$client_of_ad_row =  $this->Client_Advertisement_model->get_client_of_ad($ad_id);
			if ($client_of_ad_row->client_id != $new_client_id) {
				$this->Client_Advertisement_model->delete_by_client_ad($client_of_ad_row->client_id, $ad_id);
				//echo $this->db->last_query();die();
			}
			$this->Client_Advertisement_model->insert($data);

			$this->session->set_flashdata('message', 'Update Record Success');
			redirect(site_url('advertisements'));
		}
	}

	public function delete($id)
	{
		/* -------------------------------------------------------------------------- */
		/*                 check advertisement is in theatres , if so dont delete               */
		/* -------------------------------------------------------------------------- */
		
		$row = $this->Advertisements_model->get_by_id($id);

		if ($row) {
			$status = $this->Advertisements_model->delete($id);
			if(FALSE == $status){
				$message = "Error: Could not delete Advertisement. Already associated to theatre. Delete the entry from Theatre association list.";
			} else {
				$message = 'Delete Record Success';
			}
			$this->session->set_flashdata('message', $message);
			redirect(site_url('advertisements'));
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('advertisements'));
		}
	}

	public function _rules()
	{
		$this->form_validation->set_rules('ad_name', 'ad name', 'trim|required');
		$this->form_validation->set_rules('client_id', 'client', 'trim|required');
		
		$this->form_validation->set_rules('ad_id', 'ad_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
	}
}
