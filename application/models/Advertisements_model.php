<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Advertisements_model extends CI_Model
{

	public $table = 'advertisements';
	public $id = 'ad_id';
	public $order = 'DESC';

	function __construct()
	{
		parent::__construct();
		
	}

	// get all
	function get_all()
	{
		$this->db->order_by($this->id, $this->order);
		return $this->db->get($this->table)->result();
	}

	// get data by id
	function get_by_id($id)
	{
		$this->db->where($this->id, $id);
		return $this->db->get($this->table)->row();
	}

	// get total rows
	function total_rows($q = NULL)
	{
		$this->db->like('ad_id', $q);
		$this->db->or_like('ad_name', $q);
		$this->db->or_like('ad_type', $q);
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}
	function get_ad_type_autofill()
	{
		$query = $this->db->query("select distinct concat('\"',ad_type,'\"') as ad_type from advertisements");
		$array = $query->result_array();
		$arr = array_column($array, "ad_type");
		$arr = implode(",", $arr);
		return $arr;
	}
	function select_query()
	{
		$this->db->select("ad.*");
		$this->db->select("DATE_FORMAT(ad.created_date, '%d/%b/%Y') as created_date_formated");
		$this->db->from("$this->table as ad");
	}

	// get data with limit and search
	function get_limit_data($limit, $start = 0, $q = NULL)
	{
		$this->db->order_by($this->id, $this->order);
		$this->db->like('ad_id', $q);
		$this->db->or_like('ad_name', $q);
		$this->db->or_like('ad_type', $q);
		$this->db->or_like('created_date', $q);
		$this->db->or_like('status', $q);
		$this->db->limit($limit, $start);
		$this->select_query();
		$result = $this->db->get()->result();
		//echo $this->db->last_query();
		return $result;
	}

	// insert data
	function insert($data)
	{
		$this->db->insert($this->table, $data);
	}

	// update data
	function update($id, $data)
	{
		$this->db->where($this->id, $id);
		$this->db->update($this->table, $data);
	}

	// delete data
	function delete($id)
	{
		$this->load->model('Advertisement_theatre_model');
		$this->Advertisement_theatre_model->select_query();
		$this->db->where($this->Advertisement_theatre_model->table.".ad_id", $id);
		$result = $this->db->get()->result();
		if(!empty($result)){
			return FALSE;
		}
		$this->db->where($this->id, $id);
		$this->db->delete($this->table);

		/* -------------------------------------------------------------------------- */
		/*                 delete from client_advertisements table too                */
		/* -------------------------------------------------------------------------- */

		$this->db->where('adversment_id', $id);
		$this->db->delete('client_advertisement');
		return TRUE;
	}
}
