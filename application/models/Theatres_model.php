<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Theatres_model extends CI_Model
{

    public $table = 'theatres';
    public $id = 'theatre_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }
	function select_query(){
		$this->db->select("th.*" );
		$this->db->select("DATE_FORMAT(th.created_date, '%d/%b/%Y') as created_date_formated" );
		$this->db->from("$this->table as th");
	}

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('theatre_id', $q);
	$this->db->or_like('theatre_name', $q);
	$this->db->or_like('location', $q);
	$this->db->or_like('address', $q);
	$this->db->or_like('district', $q);
	$this->db->or_like('contact_number1', $q);
	$this->db->or_like('contact_number2', $q);
	$this->db->or_like('created_date', $q);
	$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('theatre_id', $q);
	$this->db->or_like('theatre_name', $q);
	$this->db->or_like('location', $q);
	$this->db->or_like('address', $q);
	$this->db->or_like('district', $q);
	$this->db->or_like('contact_number1', $q);
	$this->db->or_like('contact_number2', $q);
	$this->db->or_like('created_date', $q);
	$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
		$this->load->model('Advertisement_theatre_model');
		$this->Advertisement_theatre_model->select_query();
		$this->db->where($this->Advertisement_theatre_model->table.".theatre_id", $id);
		$result = $this->db->get()->result();
		if(!empty($result)){
			return FALSE;
		}
		
        $this->db->where($this->id, $id);
		$this->db->delete($this->table);
		return TRUE;
    }

}
