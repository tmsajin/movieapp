<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Common_model extends CI_Model
{

   
    function __construct()
    {
        parent::__construct();
    }
	
    function get_autofill($table,$column){
		$sql = "select distinct concat('\"',{$column},'\"') as autofilldata from $table";
		$query = $this->db->query($sql);
		$array = $query->result_array();
		$arr = array_column($array,'autofilldata');
		$arr = implode(",",$arr);
		return $arr;
	}
	
}
