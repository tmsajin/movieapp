<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Client_advertisement_model extends CI_Model
{

    public $table = 'client_advertisement';
    public $id = 'clad_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }
	function select_query(){
		$this->db->select("cladvt.*,client.client_name,
		`advertisements`.`ad_id`,
		`advertisements`.`ad_name`,
		`advertisements`.`ad_type`,
		`advertisements`.`status`,
		`advertisements`.`time_added`,
		DATE_FORMAT(advertisements.time_added, '%d/%b/%Y') as created_date_formated" );
		$this->db->from("advertisements");
		$this->db->join("$this->table as cladvt", 'cladvt.adversment_id = advertisements.ad_id','left');
		$this->db->join('client', 'client.client_id= cladvt.client_id','left');
	}
    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
    // get total rows
    function total_rows($q = NULL) {
		$this->select_query();
        $this->db->or_like('client_name', $q);
		$this->db->or_like('ad_name', $q);
		return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
		$this->select_query();
        $this->db->order_by($this->id, $this->order);
		$this->db->or_like('client_name', $q);
		$this->db->or_like('ad_name', $q);
		$this->db->limit($limit, $start);
        return $this->db->get()->result();
    }
	function get_client_of_ad($ad_id){
		$this->select_query();
		$this->db->where('advertisements.ad_id', $ad_id);
		return $this->db->get()->row();
	}
    function get_ads_of_client($client_id){
		$this->select_query();
		$this->db->where('client.client_id', $client_id);
		return $this->db->get()->result_array();
	}
    function get_menu_data_client(){
		$sql = "select   client.client_id, client.client_name from client order by client.client_name ";
		$query = $this->db->query($sql);
		$array = $query->result_array();
		$result_data = [];
		foreach($array as $item){
			$result_data[$item['client_id']]= $item['client_name'];
		}
		return $result_data;
	}
	function get_menu_data_ads(){
		$sql = "select   advertisements.ad_id, advertisements.ad_name from advertisements";
		$query = $this->db->query($sql);
		$array = $query->result_array();
		$result_data = [];
		foreach($array as $item){
			$result_data[$item['ad_id']]= $item['ad_name'];
		}
		return $result_data;
	}
    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }
	function delete_by_client_ad($client_id,$ad_id){
		$this->db->where('client_id', $client_id);
		$this->db->where('adversment_id', $ad_id);
		$this->db->delete($this->table);
	}
}
