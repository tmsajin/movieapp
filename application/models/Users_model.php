<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Users_model extends CI_Model
{

	public $table = 'users';
	public $id = 'user_id';
	public $order = 'DESC';

	function __construct()
	{
		parent::__construct();
	}

	// get all
	function get_all()
	{
		$this->db->order_by($this->id, $this->order);
		return $this->db->get($this->table)->result();
	}
	function check_login($username,$password){
		$this->db->select("users.*");
		$this->db->from("users");
		$this->db->where("username",$username);
		$this->db->where("password",$password);
		$this->db->where("status",ADV_STATUS['Active']);
		return $this->db->get()->row();
	}
	// get data by id
	function get_by_id($id)
	{
		$this->db->where($this->id, $id);
		return $this->db->get($this->table)->row();
	}

	// get total rows
	function total_rows($q = NULL)
	{
		$this->db->like('user_id', $q);
		$this->db->or_like('username', $q);
		$this->db->or_like('password', $q);
		$this->db->or_like('status', $q);
		$this->db->or_like('user_type', $q);
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	// get data with limit and search
	function get_limit_data($limit, $start = 0, $q = NULL)
	{
		$this->db->order_by($this->id, $this->order);
		$this->db->like('user_id', $q);
		$this->db->or_like('username', $q);
		$this->db->or_like('password', $q);
		$this->db->or_like('status', $q);
		$this->db->or_like('user_type', $q);
		$this->db->limit($limit, $start);
		return $this->db->get($this->table)->result();
	}


	// Login check
	function login_check($limit, $start = 0, $q = NULL)
	{
		$this->db->order_by($this->id, $this->order);
		$this->db->or_like('username', $q);
		$this->db->or_like('password', $q);
		$this->db->or_like('status', 'Active');
		$this->db->limit($limit, $start);
		return $this->db->get($this->table)->result();
	}


	// insert data
	function insert($data)
	{
		$this->db->insert($this->table, $data);
	}

	// update data
	function update($id, $data)
	{
		$this->db->where($this->id, $id);
		$this->db->update($this->table, $data);
	}

	// delete data
	function delete($id)
	{
		$this->db->where($this->id, $id);
		$this->db->delete($this->table);
	}
}
