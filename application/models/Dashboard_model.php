<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}
	function get_analytics()
	{
		$analytics =  array(
			'analytics' => array(
				'ads' => $this->db->query("SELECT count(*) as total FROM `advertisements` ")->row()->total,
				'clients' => $this->db->query("SELECT count(*) as total FROM `client` ")->row()->total,
				'theatres' => $this->db->query("SELECT count(*) as total FROM `theatres` ")->row()->total,
			)
		);
		return $analytics;
	}
	function get_analytics_chart(){
		$chart_data = $this->db->query("SELECT 
		*,
		DATE_FORMAT(time_added, '%Y') AS cy,
		DATE_FORMAT(time_added, '%b') AS cm
	FROM
		`advertisements`
	WHERE
		1
		");
		$chart_template_array_month = ['Jan'=>0, 'Feb'=>0, 'Mar'=>0, 'Apr'=>0, 'May'=>0, 'Jun'=>0, 'Jul'=>0, 'Aug'=>0, 'Sep'=>0, 'Oct'=>0, 'Nov'=>0, 'Dec'=>0];
		$chart_template_array[date("Y")] = $chart_template_array_month;
		$chart_data_input = $chart_template_array;
		
		foreach ($chart_data->result() as $row) {
			if(!isset($chart_data_input[$row->cy])){
				$chart_data_input[$row->cy] = $chart_template_array_month;
			}
			$chart_data_input[$row->cy][$row->cm] = ++$chart_data_input[$row->cy][$row->cm];
		}
		return $chart_data_input;
	}
}
                        
/* End of file dashboard_model.php */
