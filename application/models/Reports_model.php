<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Reports_model extends CI_Model
{

	public $table = 'client_advertisement';
	public $id = 'clad_id';
	public $order = 'DESC';

	function __construct()
	{
		parent::__construct();
	}
	function select_query()
	{
		$this->db->select("cladvt.*,client.client_name,advertisements.ad_name,
		theatres.theatre_name,theatres.district,DATE_FORMAT(cladvt.created_date, '%d/%b/%Y') as created_date_formated, 
		theatres.theatre_id,advertisement_theatre.time_of_show_start,
		advertisement_theatre.time_of_show_end,
		advertisement_theatre.adtheatr_id,
		DATE_FORMAT(advertisement_theatre.time_of_show_start, '%d/%b/%Y') as show_start_date_formated,
		DATE_FORMAT(advertisement_theatre.time_of_show_end, '%d/%b/%Y') as show_end_date_formated,
		DATEDIFF(advertisement_theatre.time_of_show_end,now()) as num_days_left,");
		$this->db->from("$this->table as cladvt");
		$this->db->join('client', 'client.client_id= cladvt.client_id');
		$this->db->join('advertisements', 'cladvt.adversment_id = advertisements.ad_id');
		$this->db->join('advertisement_theatre', 'advertisements.ad_id= advertisement_theatre.ad_id');
		$this->db->join('theatres', 'advertisement_theatre.theatre_id= theatres.theatre_id');
	}
	// get all
	function get_all()
	{
		$this->db->order_by($this->id, $this->order);
		return $this->db->get($this->table)->result();
	}

	// get data by id
	function get_by_id($id)
	{
		$this->db->where($this->id, $id);
		return $this->db->get($this->table)->row();
	}

	// get total rows
	function total_rows($q = NULL)
	{
		$this->select_query();
		$this->set_search_params($q);
		return $this->db->count_all_results();
	}

	// get data with limit and search
	function get_limit_data($limit, $start = 0, $q = NULL)
	{
		$this->select_query();
		$this->db->order_by($this->id, $this->order);
		$this->set_search_params($q);
		return $this->db->get()->result();
	}
	function set_search_params($q)
	{
		if (trim($q['client'])) {
			$this->db->where('client.client_id', $q['client']);
		}
		if (trim($q['advertisement'])) {
			$this->db->where('advertisements.ad_id', $q['advertisement']);
		}
		if (trim($q['theatre'])) {
			$this->db->where('theatres.theatre_id', $q['theatre']);
		}
		if (trim($q['show_critical']!='' && $q['show_warning']!='')) {
			$this->db->where('DATEDIFF(advertisement_theatre.time_of_show_end,now()) <=', DASHBOARD_REPORT_WARNING_DAYS);	
		} else
		if (trim($q['show_critical'])!='') {
			$this->db->where('DATEDIFF(advertisement_theatre.time_of_show_end,now()) <', 0);
		} else
		if (trim($q['show_warning'])!='') {
			$this->db->where('DATEDIFF(advertisement_theatre.time_of_show_end,now()) <=', DASHBOARD_REPORT_WARNING_DAYS);	
			$this->db->where('DATEDIFF(advertisement_theatre.time_of_show_end,now()) >=', 0);
		};
		
	}
	function get_menu_data_client()
	{
		$sql = "select   client.client_id, client.client_name from client";
		$query = $this->db->query($sql);
		$array = $query->result_array();
		$result_data = [];
		foreach ($array as $item) {
			$result_data[$item['client_id']] = $item['client_name'];
		}
		return $result_data;
	}
	function get_menu_data_ads()
	{
		$sql = "select   advertisements.ad_id, advertisements.ad_name from advertisements";
		$query = $this->db->query($sql);
		$array = $query->result_array();
		$result_data = [];
		foreach ($array as $item) {
			$result_data[$item['ad_id']] = $item['ad_name'];
		}
		return $result_data;
	}
	// insert data
	function insert($data)
	{
		$this->db->insert($this->table, $data);
	}

	// update data
	function update($id, $data)
	{
		$this->db->where($this->id, $id);
		$this->db->update($this->table, $data);
	}

	// delete data
	function delete($id)
	{
		$this->db->where($this->id, $id);
		$this->db->delete($this->table);
	}
}
