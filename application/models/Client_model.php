<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Client_model extends CI_Model
{

	public $table = 'client';
	public $id = 'client_id';
	public $order = 'DESC';

	function __construct()
	{
		parent::__construct();
	}

	// get all
	function get_all()
	{
		$this->db->order_by($this->id, $this->order);
		return $this->db->get($this->table)->result();
	}

	// get data by id
	function get_by_id($id)
	{
		$this->db->where($this->id, $id);
		return $this->db->get($this->table)->row();
	}

	// get total rows
	function total_rows($q = NULL)
	{
		$this->db->or_like('client_name', $q);
		$this->db->or_like('client_type', $q);
		$this->db->or_like('client_of_agency', $q);
		$this->db->or_like('location', $q);
		$this->db->or_like('address', $q);
		$this->db->or_like('district', $q);
		$this->db->or_like('contact_number', $q);
		$this->db->or_like('email_id', $q);
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	// get data with limit and search
	function get_limit_data($limit, $start = 0, $q = NULL)
	{
		$this->db->order_by($this->id, $this->order);
		$this->db->or_like('client_name', $q);
		$this->db->or_like('client_type', $q);
		$this->db->or_like('client_of_agency', $q);
		$this->db->or_like('location', $q);
		$this->db->or_like('address', $q);
		$this->db->or_like('district', $q);
		$this->db->or_like('contact_number', $q);
		$this->db->or_like('email_id', $q);
		$this->db->limit($limit, $start);
		return $this->db->get($this->table)->result();
	}

	// insert data
	function insert($data)
	{
		$this->db->insert($this->table, $data);
	}

	// update data
	function update($id, $data)
	{
		$this->db->where($this->id, $id);
		$this->db->update($this->table, $data);
	}

	// delete data
	function delete($id)
	{
		$this->load->model('Client_Advertisement_model');
		$theatres_of_ads_of_client = $this->Client_Advertisement_model->get_ads_of_client($id);

		if (!empty($theatres_of_ads_of_client)) {
			return FALSE;
		}
		$this->db->where($this->id, $id);
		$this->db->delete($this->table);

		return TRUE;
	}
}
