<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Advertisement_theatre_model extends CI_Model
{

    public $table = 'advertisement_theatre';
    public $id = 'adtheatr_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
	}
	function assign_theatres($data){
		$ad_id = $data['ad_id'];
		$this->db->query("delete from $this->table where ad_id=$ad_id");
		//die($this->db->last_query());
		foreach($data['district'] as $theatre_id){
			$insert_data['ad_id'] = $ad_id;
			$insert_data['theatre_id'] = $theatre_id;
			$insert_data['time_of_show_start'] = $data['time_of_show_start'];
			$insert_data['time_of_show_end'] = $data['time_of_show_end'];
			$this->db->insert('advertisement_theatre',$insert_data);
		}
	}
	function get_district_based_theatres_array(){
		$this->db->select("theatres.theatre_id,theatres.district,concat(theatre_name,IF(TRIM(location)<>'',CONCAT(' - ',location) ,'')) as theatre_name");
		$result = $this->db->get('theatres');
		foreach($result->result() as $item){
			$result_data[$item->district][$item->theatre_id] = $item->theatre_name;
		}
		return $result_data;
	}
	function get_selected_district_based_theatres($ad_id){
		$result_data = array();
		$this->db->select('theatres.theatre_id');
		$this->db->from('theatres');
		$this->db->join('advertisement_theatre as ath','ath.theatre_id=theatres.theatre_id');
		$this->db->join('advertisements','advertisements.ad_id=ath.ad_id');
		$this->db->where('advertisements.ad_id',$ad_id);
		$result = $this->db->get();
		foreach($result->result() as $item){
			$result_data[$item->theatre_id] = $item->theatre_id;
		}
		return $result_data;
	}
	function select_query(){
		$this->db->select("$this->table.*,theatres.theatre_name,theatres.district,advertisements.ad_name,
		DATE_FORMAT($this->table.created, '%d/%b/%Y') as created_date_formated,
		DATE_FORMAT($this->table.time_of_show_start, '%d/%b/%Y') as time_of_show_start_date_formated,
		DATE_FORMAT($this->table.time_of_show_end, '%d/%b/%Y') as time_of_show_end_date_formated,
		client.client_name
		" );
		$this->db->from("$this->table");
		$this->db->join('theatres', "theatres.theatre_id= $this->table.theatre_id");
		$this->db->join('advertisements', "$this->table.ad_id = advertisements.ad_id");
		$this->db->join('client_advertisement', "advertisements.ad_id = client_advertisement.adversment_id");
		$this->db->join('client', "client_advertisement.client_id = client.client_id");
	}
	
    function get_menu_data_advertisement(){
		$sql = "select   advertisements.ad_id, advertisements.ad_name from advertisements order by advertisements.ad_name" ;
		$query = $this->db->query($sql);
		$array = $query->result_array();
		$result_data = [];
		foreach($array as $item){
			$result_data[$item['ad_id']]= $item['ad_name'];
		}
		return $result_data;
	}
	function get_menu_data_theatres(){
		$sql = "select   theatres.theatre_id, theatres.theatre_name from theatres order by  theatres.theatre_name ";
		return $this->_build_get_menu_theatre_output($sql);
	}
    function get_menu_data_theatres_with_location(){
		$sql = "select   theatres.theatre_id, concat(theatres.theatre_name,' ',theatres.location) as theatre_name from theatres order by  theatres.theatre_name ";
		return $this->_build_get_menu_theatre_output($sql);
	}
    function get_menu_data_theatres_with_district(){
		$sql = "select   theatres.theatre_id, 
		concat(theatres.theatre_name,' - ',theatres.district,' ',theatres.location) as theatre_name from theatres order by  theatre_name";
		return $this->_build_get_menu_theatre_output($sql);
	}
	function _build_get_menu_theatre_output($sql){
		$query = $this->db->query($sql);
		$array = $query->result_array();
		$result_data = [];
		foreach($array as $item){
			$result_data[$item['theatre_id']]= $item['theatre_name'];
		}
		return $result_data;
	}
    
	function get_ads_of_client($client_id){
		$this->select_query();
		$this->db->where('client.client_id',$client_id);
		return $this->db->get()->result();
	}
    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
    // get total rows
    function total_rows($q = NULL) {
		$this->select_query();
        $this->db->or_like('ad_name', $q);
		$this->db->or_like('theatre_name', $q);
		$this->db->or_like('client.client_name', $q);
		
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
		$this->select_query();
        $this->db->order_by($this->id, $this->order);
        $this->db->or_like('ad_name', $q);
		$this->db->or_like('theatre_name', $q);
		$this->db->or_like('client.client_name', $q);
		$this->db->limit($limit, $start);
        return $this->db->get()->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}
