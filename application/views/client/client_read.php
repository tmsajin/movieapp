
        <h2 style="margin-top:0px">Client Details</h2>
        <table class="table">
	    <tr><td>Client Name</td><td><?php echo $client_name; ?></td></tr>
	    <tr><td>Client Type</td><td><?php echo $client_type; ?></td></tr>
		<?php
		if($client_type==COMMON_CLIENT_TYPES["Agency"]){?> 
			<tr>
				<td>Client Of Agency</td>
				<td><?php echo $client_of_agency; ?></td>
			</tr>
			<?php 
		}
		?>
		<tr><td>Location</td><td><?php echo $location; ?></td></tr>
		<tr><td>Address</td><td><?php echo $address; ?></td></tr>
	    <tr><td>District</td><td><?php echo $district; ?></td></tr>
	    <tr><td>Contact Number</td><td><?php echo $contact_number; ?></td></tr>
	    <tr><td>Email Id</td><td><?php echo $email_id; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('client') ?>" class="btn btn-default">Cancel</a></td></tr>
	</table>
