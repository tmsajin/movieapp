<h2 style="margin-top:0px">Client <?php echo $button ?></h2>
<form action="<?php echo $action; ?>" method="post">
	<div class="form-group">
		<label for="varchar">Client Name <?php echo form_error('client_name') ?></label>
		<input type="text" class="form-control" name="client_name" id="client_name" placeholder="Client Name" value="<?php echo $client_name; ?>" />
	</div>
	<div class="form-group">
		<label for="varchar">Client Type <?php echo form_error('client_type') ?></label>
		<?php echo form_dropdown("client_type", COMMON_CLIENT_TYPES, $client_type, array("id" => "client_type", "class" => "form-control")) ?>
	</div>
	<div class="form-group">
		<label for="varchar">Client of Agency (If client type is agency, mention the client name which the agency handling)<?php echo form_error('client_of_agency') ?></label>
		<input type="text" <?php echo COMMON_CLIENT_TYPES["Agency"] != $client_type ? "Disabled" : ''; ?>
		class="form-control" name="client_of_agency" id="client_of_agency" placeholder="Client of Agency" value="<?php echo $client_of_agency; ?>" />
	</div>

	<div class="form-group">
		<label for="varchar">Location <?php echo form_error('location') ?></label>
		<input type="text" class="form-control" name="location" id="location" placeholder="Location" value="<?php echo $location; ?>" />
	</div>
	<div class="form-group">
		<label for="address">Address <?php echo form_error('address') ?></label>
		<textarea class="form-control" rows="3" name="address" id="address" placeholder="Address"><?php echo $address; ?></textarea>
	</div>
	<div class="form-group">
		<label for="varchar">District <?php echo form_error('district') ?></label>
		<?php echo form_dropdown("district", array(''=>'-Select-')+COMMON_MOVIE_DISTRICTS, $district, array("id" => "district", "class" => "form-control")) ?>

	</div>
	<div class="form-group">
		<label for="varchar">Contact Number <?php echo form_error('contact_number') ?></label>
		<input type="text" class="form-control" name="contact_number" id="contact_number" placeholder="contact_number" value="<?php echo $contact_number; ?>" />
	</div>
	<div class="form-group">
		<label for="varchar">Email Id <?php echo form_error('email_id') ?></label>
		<input type="text" class="form-control" name="email_id" id="email_id" placeholder="Email Id" value="<?php echo $email_id; ?>" />
	</div>
	<input type="hidden" name="client_id" value="<?php echo $client_id; ?>" />
	<button type="submit" class="btn btn-primary"><?php echo $button ?></button>
	<a href="<?php echo site_url('client') ?>" class="btn btn-default">Cancel</a>
</form>

<script>
	$(document).ready(function() {


		/* -------------------------------------------------------------------------- */
		/*                 Autocomplete script for Client form                 */
		/* -------------------------------------------------------------------------- */
		// Autocomplete
		var availableLocation = [<?php echo $location_autofill; ?>];

		$("#location").autocomplete({
			source: availableLocation
		});
		$("#client_type").on('change', function() {
			if (this.value == 'Agency') {
				$("#client_of_agency").removeAttr("disabled"); 
			} else {
				$('#client_of_agency').attr("disabled", "disabled");
			}
		});;

	});
</script>
