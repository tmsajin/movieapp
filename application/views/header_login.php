<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>Ads Manager v.1</title>

	<link rel="canonical" href="<?php echo base_url(); ?>">
	<!-- Bootstrap core CSS -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/bootstrap-4.0.0-dist/css/bootstrap.min.css">
	<!-- Custom styles for this template -->
	<link href="<?php echo base_url(); ?>/assets/dashboard.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/jquery-ui-1.12.1.custom/jquery-ui.css" />

	<script src="<?php echo base_url(); ?>/assets/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>/assets/popper.min.js"></script>
	<script src="<?php echo base_url(); ?>/assets/bootstrap-4.0.0-dist/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>/assets/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
	<script src="<?php echo base_url(); ?>/assets/feather.min.js"></script>

<body>
	<nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
		<a class="navbar-brand col-sm-3 col-md-2 mr-0" href="<?php echo base_url() ?>">Ads Manager v.1</a>

		
	</nav>

	<div class="container-fluid">

		<div class="row">


			<main role="main" class="col-md-12 ml-sm-auto col-lg-12 pt-3 px-4">
