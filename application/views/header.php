<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>Ads Manager v.1</title>

	<link rel="canonical" href="<?php echo base_url(); ?>">
	<!-- Bootstrap core CSS -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/bootstrap-4.0.0-dist/css/bootstrap.min.css">
	<!-- Custom styles for this template -->
	<link href="<?php echo base_url(); ?>/assets/dashboard.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/jquery-ui-1.12.1.custom/jquery-ui.css" />
	<script src="<?php echo base_url(); ?>/assets/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>/assets/popper.min.js"></script>
	<script src="<?php echo base_url(); ?>/assets/bootstrap-4.0.0-dist/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>/assets/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
	<script src="<?php echo base_url(); ?>/assets/feather.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/common.js"></script>
	<script src="<?php echo base_url(); ?>assets/bootstrap-multiselect.min.js"></script>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap-multiselect.css" type="text/css" />
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url(); ?>apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url(); ?>favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>favicon-16x16.png">
	<link rel="manifest" href="<?php echo base_url(); ?>site.webmanifest">
	<link href="<?php echo base_url(); ?>assets/lou-multi-select/css/multi-select.css" media="screen" rel="stylesheet" type="text/css">
	<script src="<?php echo base_url(); ?>assets/lou-multi-select/js/jquery.multi-select.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>assets/jquery.quicksearch.js" type="text/javascript"></script>

<body>
	<nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
		<a class="navbar-brand col-sm-3 col-md-2 mr-0" href="<?php echo base_url() ?>">Ads Manager v.1</a>

		<ul class="navbar-nav px-3">
			<li class="nav-item text-nowrap">
				<a class="nav-link" href="<?php echo base_url() ?>login/logout">Sign out</a>
			</li>
		</ul>
	</nav>

	<div class="container-fluid">

		<div class="row">
			<nav class="col-md-2 d-none d-md-block bg-light sidebar">
				<div class="sidebar-sticky">
					<ul class="nav flex-column">
						<li class="nav-item">
							<a class="nav-link" href="<?php echo base_url() ?>">
								<span data-feather="home"></span>
								Dashboard <span class="sr-only">(current)</span>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link <?php echo dslp_get_active_link('advertisements');?>"   href="<?php echo base_url() ?>advertisements">
								<span data-feather="file"></span>
								Advertisements
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link  <?php echo dslp_get_active_link('client');?>" href="<?php echo base_url() ?>client">
								<span data-feather="user-check"></span>
								Clients
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link <?php echo dslp_get_active_link('theatres');?>" href="<?php echo base_url() ?>theatres">
								<span data-feather="airplay"></span>
								Theaters
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link  <?php echo $this->uri->segment(1)=='advertisement_theatre' && $this->uri->segment(2)!='assign_district'?'active':null ;?>" href="<?php echo base_url() ?>advertisement_theatre">
								<span data-feather="camera"></span>
								Ads in theatres
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link   <?php echo $this->uri->segment(1)=='advertisement_theatre' && $this->uri->segment(2)=='assign_district'?'active':null ;?>" href="<?php echo base_url() ?>advertisement_theatre/assign_district">
								<span data-feather="film"></span>
								Ads to Districts
							</a>
						</li>
						<?php
						$session_data = $this->session->userdata('login_session');

						if ($session_data->user_type == 'Admin') { ?>
							<li class="nav-item">
								<a class="nav-link <?php  echo $this->uri->segment(1)=='Users'?'active':null;?>" href="<?php echo base_url() ?>Users">
									<span data-feather="users"></span>
									Users
								</a>
							</li>
						<?php } ?>

					</ul>

					<h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
						<span>Reports section</span>

					</h6>
					<ul class="nav flex-column mb-2">
						<li class="nav-item">
							<a class="nav-link  <?php  echo $this->uri->segment(1)=='reports_general'?'active':null;?>" href="<?php echo base_url() ?>reports_general">
								<span data-feather="file-text"></span>
								Reports - General
							</a>
						</li>

					</ul>
				</div>
			</nav>

			<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
