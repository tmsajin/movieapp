
        <h2 style="margin-top:0px">Advertisements List</h2>
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                <?php echo anchor(site_url('advertisements/create'),'Create', 'class="btn btn-primary"'); ?>
            </div>
            <div class="col-md-4 text-center">
				<div style="margin-top: 8px" id="message" class="alert 
				<?php 
				$message_action = $this->session->userdata('message');
				if(strpos($message_action,'Error:') !== FALSE)
				echo  ' alert-danger'; 
				else
				if($message_action <> '')
					echo  ' alert-info';
				else echo 'd-none';
				?>">
                    <?php echo $message_action <> '' ? $message_action: ''; ?>
                </div>
            </div>
            <div class="col-md-1 text-right">
            </div>
            <div class="col-md-3 text-right">
                <form action="<?php echo site_url('advertisements/index'); ?>" class="form-inline" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                        <span class="input-group-btn">
                            <?php 
                                if ($q <> '')
                                {
                                    ?>
                                    <a href="<?php echo site_url('advertisements'); ?>" class="btn btn-default">Reset</a>
                                    <?php
                                }
                            ?>
                          <button class="btn btn-primary" type="submit">Search</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
        <table class="table table-bordered" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Ad Name</th>
		<th>Ad Type</th>
		<th>Client/Agency Name</th>
		<th>Created Date</th>
		<th>Action</th>
            </tr><?php
            foreach ($advertisements_data as $advertisements)
            {
                ?>
                <tr>
			<td width="80px"><?php echo ++$start ?></td>
			<td><?php echo $advertisements->ad_name ?></td>
			<td><?php echo $advertisements->ad_type ?></td>
			<td><?php echo $advertisements->client_name?anchor('client/read/'.$advertisements->client_id,$advertisements->client_name):''?></td>
			<td><?php echo $advertisements->created_date_formated ?></td>
			<td style="text-align:center" width="200px">
				<?php 
				echo anchor(site_url('advertisements/read/'.$advertisements->ad_id),'View'); 
				echo ' | '; 
				echo anchor(site_url('advertisements/update/'.$advertisements->ad_id),'Update'); 
				echo ' | '; 
				echo anchor(site_url('advertisements/delete/'.$advertisements->ad_id),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
				?>
			</td>
		</tr>
                <?php
            }
            ?>
        </table>
        <div class="row">
            <div class="col-md-6">
                <a href="#" class="btn btn-primary">Total Record : <?php echo $total_rows ?></a>
	    </div>
            <div class="col-md-6 text-right">
                <?php echo $pagination ?>
            </div>
        </div>
   