
        <h2 style="margin-top:0px">Advertisements <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="ad_name">Ad Name <?php echo form_error('ad_name') ?></label>
            <input type="text" class="form-control" name="ad_name" id="ad_name" placeholder="Ad Name" value="<?php echo $ad_name; ?>" />
		</div>
		
	    <div class="form-group">
            <label for="ad_type">Ad Type <?php echo form_error('ad_type') ?></label>
            <input type="text" class="form-control" name="ad_type" id="ad_type" placeholder="Ad Type" value="<?php echo $ad_type; ?>" />
		</div>
		<div class="form-group">
            <label for="client_id">Client  <?php echo form_error('client_id') ?></label>
			<?php echo form_dropdown("client_id",$clients,$client_id,array("id"=>"client_id","class"=>"form-control"))?>
        </div>
	    
		<!--
	    <div class="form-group">
            <label for="enum">Status <?php echo form_error('status') ?></label>
			<?php echo form_dropdown("status",ADV_STATUS,$status,array("id"=>"status","class"=>"form-control"))?>
        </div> -->
		<input type="hidden" name="status" value="Active" /> 
	    <input type="hidden" name="ad_id" value="<?php echo $ad_id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('advertisements') ?>" class="btn btn-default">Cancel</a>
	</form>

	<script>
	$(document).ready(function() {


/* -------------------------------------------------------------------------- */
/*                 Autocomplete script for Advertisement form                 */
/* -------------------------------------------------------------------------- */
	// Autocomplete
	var availableTags = [<?php echo $autofil_data;?>];
 
	$("#ad_type").autocomplete({
		source: availableTags
	});
	 

});

	</script>
