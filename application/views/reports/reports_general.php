<h2>General Reports</h2>
<form action="" class="mt-5" method="get">
	<div class="row">
		<div class="form-group col-sm-3 col-xs-6">
			<label for="theatre_search">Choose Theatre</label>
			<?php echo form_dropdown("theatre_search", $t_data, $q['theatre'], array("id" => "theatre_search", "class" => "form-control")) ?>
		</div>
		<div class="form-group col-sm-3 col-xs-6">
			<label for="client_search">Choose Client</label>
			<?php echo form_dropdown("client_search", $client_data, $q['client'], array("id" => "client_search", "class" => "form-control")) ?>
		</div>
		<div class="form-group col-sm-3 col-xs-6">
			<label for="client_search">Choose Advertisement</label>
			<?php echo form_dropdown("advertisement_search", $a_data, $q['advertisement'], array("id" => "theatre_search", "class" => "form-control")) ?>
		</div>
		<div class="form-group col-sm-6 col-xs-6">
				<label class="checkbox-inline">
					<?php echo form_checkbox('show_critical','show_critical',$this->input->get('show_critical'));?> Show Critical only </label>
				<label  class="checkbox-inline">
				<?php echo form_checkbox('show_warning','show_warning',$this->input->get('show_warning'));?> Show Warning only </label>
		</div>
		<div class="form-group col-sm-3 col-xs-6">
			<div class="input-group-btn">
				<button class="btn btn-primary" name="submit" value="Search" type="submit">Search</button>
				<button name="submit" class="btn btn-info" value="Download" type="submit">Download</button>
				<button class="btn btn-info" name="submit" value="Reset" type="submit">Reset</button>
			</div>
		</div>
	</div>
</form>
<table class="table table-bordered  reports-general mb-5">
	<tr>
		<th>No</th>
		<th>Client </th>
		<th>Advertisment </th>
		<th>Theatre</th>
		<th>District</th>
		<th>Ad start date</th>
		<th>Ad end date</th>
		<th>Action</th>
	</tr>
	<?php
	foreach ($client_advertisement_data as $client_advertisement) {
	?>
		<tr class="<?php if ($client_advertisement->num_days_left < 0 || $client_advertisement->num_days_left == null)
						echo 'bg-danger';
					else if ($client_advertisement->num_days_left <= DASHBOARD_REPORT_WARNING_DAYS)
						echo 'bg-warning'; ?>">
			<td width="80px"><?php echo ++$start ?></td>
			<td>
				<?php
				echo anchor(site_url('client/read/' . $client_advertisement->client_id), $client_advertisement->client_name);
				?>
			</td>
			<td>
				<?php
				echo anchor(site_url('advertisements/read/' . $client_advertisement->adversment_id), $client_advertisement->ad_name);
				?>
			</td>
			<td>
				<?php
				echo anchor(site_url('theatres/read/' . $client_advertisement->theatre_id), $client_advertisement->theatre_name);
				?>
			</td>
			<td>
				<?php
				echo $client_advertisement->district;
				?>
			</td>
			<td>
				<?php
				echo $client_advertisement->show_start_date_formated;
				?>
			</td>
			<td>
				<?php
				echo $client_advertisement->show_end_date_formated;
				?>
			</td>
			<td>
				<?php
				echo anchor(site_url('advertisement_theatre/update/' . $client_advertisement->adtheatr_id), 'Update');
				?>
			</td>
		</tr>
	<?php
	}
	?>
</table>
<div class="row">
	<div class="col-md-6">
		<a href="#" class="btn btn-primary">Total Record : <?php echo $total_rows ?></a>
	</div>
	<div class="col-md-6 text-right">
		<?php echo $pagination ?>
	</div>
</div>
