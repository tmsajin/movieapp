<link rel="stylesheet" href="<?php echo base_url();?>/assets/font-awesome-4.7.0/css/font-awesome.css
">

<div class="jumbotron">
<div class="row w-100">
        <div class="col-md-4">
            <div class="card border-info mx-sm-1 p-3">
                <div class="card border-info shadow text-info p-3 my-card" ><span class="fa fa-object-group" aria-hidden="true"></span></div>
                <div class="text-info text-center mt-3"><h4>Advertisements</h4></div>
                <div class="text-info text-center mt-2"><h1><?php echo $ads; ?></h1></div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card border-success mx-sm-1 p-3">
                <div class="card border-success shadow text-success p-3 my-card"><span class="fa fa-user-circle" aria-hidden="true"></span></div>
                <div class="text-success text-center mt-3"><h4>Clients</h4></div>
                <div class="text-success text-center mt-2"><h1><?php echo $clients; ?></h1></div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card border-danger mx-sm-1 p-3">
                <div class="card border-danger shadow text-danger p-3 my-card" ><span class="fa fa-desktop" aria-hidden="true"></span></div>
                <div class="text-danger text-center mt-3"><h4>Theatres</h4></div>
                <div class="text-danger text-center mt-2"><h1><?php echo $theatres; ?></h1></div>
            </div>
        </div>
     </div>
</div>
