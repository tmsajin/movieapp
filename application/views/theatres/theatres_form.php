
        <h2 style="margin-top:0px">Theatres <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Theatre Name <?php echo form_error('theatre_name') ?></label>
            <input type="text" class="form-control" name="theatre_name" id="theatre_name" placeholder="Theatre Name" value="<?php echo $theatre_name; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Location <?php echo form_error('location') ?></label>
            <input type="text" class="form-control" name="location" id="location" placeholder="Location" value="<?php echo $location; ?>" />
        </div>
	    <div class="form-group">
            <label for="address">Address <?php echo form_error('address') ?></label>
            <textarea class="form-control" rows="3" name="address" id="address" placeholder="Address"><?php echo $address; ?></textarea>
        </div>
	    <div class="form-group">
            <label for="varchar">District <?php echo form_error('district') ?></label>
			<?php echo form_dropdown("district",array(''=>'-Select-')+COMMON_MOVIE_DISTRICTS,$district,array("id"=>"district","class"=>"form-control"))?>
        </div>
	    <div class="form-group">
            <label for="varchar">Contact Number1 <?php echo form_error('contact_number1') ?></label>
            <input type="text" class="form-control" name="contact_number1" id="contact_number1" placeholder="Contact Number1" value="<?php echo $contact_number1; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Contact Number2 <?php echo form_error('contact_number2') ?></label>
            <input type="text" class="form-control" name="contact_number2" id="contact_number2" placeholder="Contact Number2" value="<?php echo $contact_number2; ?>" />
        </div>
	    <div class="form-group">
            <label for="timestamp">Created Date <?php echo form_error('created_date') ?></label>
            <input type="text" class="form-control popup_date_field" name="created_date" id="created_date" placeholder="Created Date" value="<?php echo $created_date; ?>" />
        </div>
	    <input type="hidden" name="theatre_id" value="<?php echo $theatre_id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('theatres') ?>" class="btn btn-default">Cancel</a>
	</form>

	<script>
	$(document).ready(function() {


/* -------------------------------------------------------------------------- */
/*                 Autocomplete script for Client form                 */
/* -------------------------------------------------------------------------- */
	// Autocomplete
	var availableTheatres = [<?php echo $theatre_type_autofill;?>];
	var availableLocation = [<?php echo $location_autofill;?>];
 	
	$("#theatre_name").autocomplete({
		source: availableTheatres
	});
	$("#location").autocomplete({
		source: availableLocation
	});
	 

});

	</script>
