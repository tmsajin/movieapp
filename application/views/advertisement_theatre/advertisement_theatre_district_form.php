<h2 style="margin-top:0px">Assign Ads to District </h2>
<form action="<?php echo $action; ?>" method="post" id="ad_theatre_district_form" class="ad_theatre_district_form">
	<div class="form-group">
		<label for="ad_id">Ad Name <?php echo form_error('ad_id') ?></label>
		<?php echo form_dropdown("ad_id", $advertisement_autofill, $ad_id, array("id" => "ad_id", "class" => "form-control")) ?>
	</div>
	<div class="form-group">
		<label for="district">District (Minimum one theatre required. If none selected, it will restore the previous saved theatre.) <?php echo form_error('district[]') ?></label>
		<?php echo form_multiselect(
			"district[]",
			$district_based_theatres_array,
			$district_based_theatres_selected,
			array("id" => "district", "class" => "form-control district_multiselect")
		) ?>
	</div>
	<div class="form-group">
		<label for="time_of_show_start">Date Of Show start <?php echo form_error('time_of_show_start') ?></label>
		<input type="text" class="form-control popup_date_field" name="time_of_show_start" id="time_of_show_start" placeholder="Date Of Show start" value="<?php echo $time_of_show_start; ?>" />
	</div>
	<div class="form-group">
		<label for="time_of_show_end">Date Of Show end <?php echo form_error('time_of_show_end') ?></label>
		<input type="text" class="form-control popup_date_field" name="time_of_show_end" id="time_of_show_end" placeholder="Date Of Show end" value="<?php echo $time_of_show_start; ?>" />
	</div>
	<input type="submit" class="btn btn-primary" value="Associate Ad To District">
	<input type="hidden" id="form_hidden_menu_change_action" name="form_hidden_menu_change_action">
	<a href="<?php echo site_url('advertisement_theatre') ?>" class="btn btn-default">Cancel</a>
</form>
<script>
	$(document).ready(function() {
		$('#ad_id').change(function() {
			$('#form_hidden_menu_change_action').val('menu_change');
			$('#ad_theatre_district_form').submit();
		});
		$('#district').change(function() {
			//$('#ad_theatre_district_form').submit();
		});
		$('#district').multiSelect({
			selectableHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='Search by theatre name'>",
			selectionHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='Search by theatre name'>",
			selectableFooter: "<span class=\"small\">Available Theatres</span>",
			selectionFooter: "<span class=\"small\">Selected Theatres(Minimum 1 item required)</span>",
			afterInit: function(ms) {
				var that = this,
					$selectableSearch = that.$selectableUl.prev(),
					$selectionSearch = that.$selectionUl.prev(),
					selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
					selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

				that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
					.on('keydown', function(e) {
						if (e.which === 40) {
							that.$selectableUl.focus();
							return false;
						}
					});

				that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
					.on('keydown', function(e) {
						if (e.which == 40) {
							that.$selectionUl.focus();
							return false;
						}
					});
			},
			afterSelect: function() {
				this.qs1.cache();
				this.qs2.cache();
			},
			afterDeselect: function() {
				this.qs1.cache();
				this.qs2.cache();
			}
		});
	});
</script>
