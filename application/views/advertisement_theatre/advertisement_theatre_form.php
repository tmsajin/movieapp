
        <h2 style="margin-top:0px">Ads in theatre <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="bigint">Ad Name <?php echo form_error('ad_id') ?></label>
			<?php echo form_dropdown("ad_id",$advertisement_autofill,$ad_id,array("id"=>"ad_id","class"=>"form-control"))?>
        </div>
	    <div class="form-group">
            <label for="bigint">Theatre Name <?php echo form_error('theatre_id') ?></label>
			<?php echo form_dropdown("theatre_id",$theatre_type_autofill,$theatre_id,array("id"=>"theatre_id","class"=>"form-control"))?>
        </div>
	    <div class="form-group">
            <label for="datetime">Date Of Show start<?php echo form_error('time_of_show_start') ?></label>
            <input type="text" class="form-control popup_date_field" name="time_of_show_start" id="time_of_show_start" placeholder="Date Of Show Start" value="<?php echo $time_of_show_start; ?>" />
        </div>
	    <div class="form-group">
            <label for="datetime">Date Of Show end <?php echo form_error('time_of_show_end') ?></label>
            <input type="text" class="form-control popup_date_field" name="time_of_show_end" id="time_of_show_end" placeholder="Date Of Show End" value="<?php echo $time_of_show_end; ?>" />
        </div>
	    <input type="hidden" name="adtheatr_id" value="<?php echo $adtheatr_id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('advertisement_theatre') ?>" class="btn btn-default">Cancel</a>
	</form>
