
        <h2 style="margin-top:0px">Ads in theatre</h2>
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                <?php echo anchor(site_url('advertisement_theatre/create'),'Create', 'class="btn btn-primary"'); ?>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 8px" id="message"  class="alert alert-info <?php echo $this->session->userdata('message') <> '' ? : 'd-none'; ?>">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-1 text-right">
            </div>
            <div class="col-md-3 text-right">
                <form action="<?php echo site_url('advertisement_theatre/index'); ?>" class="form-inline" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                        <span class="input-group-btn">
                            <?php 
                                if ($q <> '')
                                {
                                    ?>
                                    <a href="<?php echo site_url('advertisement_theatre'); ?>" class="btn btn-default">Reset</a>
                                    <?php
                                }
                            ?>
                          <button class="btn btn-primary" type="submit">Search</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
        <table class="table table-bordered" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Ad Name</th>
		<th>Theatre Name</th>
		<th>District</th>
		<th>Date Of Show start</th>
		<th>Date Of Show end</th>
		<th>Client/Agency</th>
		<th>Action</th>
            </tr><?php
            foreach ($advertisement_theatre_data as $advertisement_theatre)
            {
                ?>
                <tr>
			<td width="80px"><?php echo ++$start ?></td>
			<td><?php echo $advertisement_theatre->ad_name ?></td>
			<td><?php echo $advertisement_theatre->theatre_name ?></td>
			<td><?php echo $advertisement_theatre->district ?></td>
			<td><?php echo $advertisement_theatre->time_of_show_start_date_formated ?></td>
			<td><?php echo $advertisement_theatre->time_of_show_end_date_formated ?></td>
			<td><?php echo $advertisement_theatre->client_name ?></td>
			<td style="text-align:center" width="200px">
				<?php 
				echo anchor(site_url('advertisement_theatre/read/'.$advertisement_theatre->adtheatr_id),'View'); 
				echo ' | '; 
				echo anchor(site_url('advertisement_theatre/update/'.$advertisement_theatre->adtheatr_id),'Update'); 
				echo ' | '; 
				echo anchor(site_url('advertisement_theatre/delete/'.$advertisement_theatre->adtheatr_id),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
				?>
			</td>
		</tr>
                <?php
            }
            ?>
        </table>
        <div class="row">
            <div class="col-md-6">
                <a href="#" class="btn btn-primary">Total Record : <?php echo $total_rows ?></a>
	    </div>
            <div class="col-md-6 text-right">
                <?php echo $pagination ?>
            </div>
        </div>
