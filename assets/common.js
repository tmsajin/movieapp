$(document).ready(function() {

/* -------------------------------------------------------------------------- */
/*                      Common script for calendar poupup                     */
/* -------------------------------------------------------------------------- */

//DOM manipulation code
$('.popup_date_field').datepicker({
    inline: false,
	dateFormat: 'yy-mm-dd',
	changeYear: true,
	changeMonth: true
});
$(".popup_date_field").attr("autocomplete", "nope");

});
